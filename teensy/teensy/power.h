#pragma once

#include "coroutine.h"

namespace teensy {

template<class Delegate>
struct power
{   
    Delegate & _delegate;
    int _pin; // tied to on-off pin

    FLASHMEM void shutdown()
    {   _delegate.pre_shutdown();
        // https://github.com/FrankBoesing/T4_PowerButton
        SNVS_LPCR |= SNVS_LPCR_TOP; // request power down
        asm volatile ("dsb"); // instruction sync barrier
        while(1) { asm ("wfi"); } // wait for interrupt
    }

    co_delay _delay_us;
    int _hold_ms = 0;

    static constexpr int poll_ms = 200;
    static constexpr int shutdown_ms = 2000;

    COROUTINE()
    {   pinMode(_pin, INPUT_PULLUP);
        while(1)
        {   AWAIT(_delay_us(poll_ms * 1000));
            _hold_ms += poll_ms;
            bool request_off = !digitalRead(_pin);
            if(!request_off) { _hold_ms = 0; continue; }
            if(_hold_ms >= shutdown_ms) { _hold_ms = 0; shutdown(); }
        }
        COFINISH();
    }
};

} // power

