#pragma once

#define COROUTINE(...) int _cojump = 0; bool operator()(__VA_ARGS__) { switch(_cojump) { case 0:
#define YIELD() _cojump = __LINE__; return false; case __LINE__: 
#define COFINISH() } _cojump = 0; } return true;
#define AWAIT(x) while(!(x)) { YIELD(); }

struct co_delay
{   uint32_t _start;
    COROUTINE(uint32_t usec)
    {   _start = micros();
        AWAIT(micros() - _start >= usec);
        COFINISH();
    }
};