#pragma once

#include "coroutine.h"

namespace teensy {
    
template<class Delegate>
struct button
{
    Delegate & delegate;
    int pin;
    int bounce_ms;

    int hold = 0;
    bool state = false;
    co_delay _delay_us;

    COROUTINE()
    {
        while(1)
        {
            AWAIT(_delay_us(1000));
            
            if(hold > 0) { hold--; }
            bool nstate = digitalRead(pin);
            if(state != nstate && hold == 0)
            {   state = nstate;
                hold = bounce_ms;
                delegate.update_button(pin, state);
            }
        }
        COFINISH();
    }
};

} // teensy