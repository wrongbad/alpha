#pragma once

#include <Audio.h>

namespace teensy {

template<class Delegate>
struct audio : public AudioStream
{
    static constexpr int block_size = AUDIO_BLOCK_SAMPLES;
    
    Delegate & _delegate;

    float buffer[block_size];

    AudioControlSGTL5000 codec;
    AudioOutputI2S  audio_out;
    AudioConnection patch[2] = {
        {*this, 0, audio_out, 0},
        {*this, 0, audio_out, 1}
    };

    audio(Delegate & delegate)
    :   AudioStream(0, nullptr),
        _delegate(delegate)
    {}
    
    void setup()
    {   AudioMemory(4);
        codec.enable();
    //    codec.volume(0.05);
    //    codec.dacVolume(0.5);
        codec.lineOutLevel(31);
    }
    
    void update(void) override
    {   audio_block_t * out = allocate();
        if(!out) { return; }
        
        _delegate.render(buffer, block_size);
        
        for(int i=0 ; i<block_size ; i++)
        {   int32_t q = buffer[i] * (1<<12);
            q = min(q, INT16_MAX);
            q = max(q, INT16_MIN);
            out->data[i] = q;
        }
        
        transmit(out, 0);
        release(out);
    }
};

} // teensy