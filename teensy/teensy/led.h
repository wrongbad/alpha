#pragma once

#include <OctoWS2811.h>
#include "coroutine.h"

namespace teensy {

auto delay_ms = delay;

// template<int npins, int nleds>
struct led
{
    static const int config = WS2811_GRB | WS2811_800kHz;

    // uint32_t draw_mem[(nleds * npins * 3 + sizeof(uint32_t) - 1) / sizeof(uint32_t)] = {0};

    void * dma_mem;
    OctoWS2811 leds;

    // mgr(byte const (&pins)[npins]/*, void * dma_mem*/)
    // :   leds(nleds, dma_mem, nullptr, config, npins, pins)
    // {}
    led(int npins, int nleds, byte const* pins)
    :   dma_mem(malloc(nleds * npins * 3)),
        leds(nleds, dma_mem, nullptr, config, npins, pins)
    {}
    ~led()
    {   free(dma_mem);
    }
    void setup()
    {   leds.begin();
        leds.show();
    }
    void shutdown()
    {   for(int i=0 ; i<leds.numPixels() ; i++)
        {   leds.setPixel(i, 0);
            leds.show();
        }
        delay_ms(50);
    }
    void set_color(int id, uint32_t color)
    {   leds.setPixel(id, color);
    }

    co_delay _delay_us;
    COROUTINE()
    {   while(1)
        {   AWAIT(_delay_us(20000));
            leds.show();
        }
        COFINISH();
    }
};

} // teensy