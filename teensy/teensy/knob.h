#pragma once

#include <cmath>
#include "coroutine.h"

namespace teensy {

// bitbang i2c mess to read 
// 7 hall effect sensors with the same address
// using shared clock and paralell data pins
// coroutines make it slightly less bad
// todo separate hall-sensor and knob logic
template<class Delegate, int devs = 7>
struct knob
{   
    Delegate & delegate;
    byte const clk_pin;
    byte const (&dat_pin)[devs];

    static const byte dev_addr = 53;
    static const int readlen = 7;
    static const uint32_t dt = 5; // microseconds per half-clock

    enum i2c_op
    {   ACK = 0,
        NACK = 1
    };
    enum i2c_ack
    {   WRITE = 0,
        READ = 1
    };

    bool pin(int pin, bool up)
    {   if(up)
        {   pinMode(pin, INPUT_PULLUP);
            digitalWrite(pin, 1); // TODO required?
            return digitalRead(pin) == 1;
        }
        else
        {   digitalWrite(pin, 0);
            pinMode(pin, OUTPUT);
            return digitalRead(pin) == 0;
        }
    }
    bool clk(bool up)
    {   return pin(clk_pin, up);
    }
    void write_all(bool up)
    {   for(int idx=0 ; idx<devs ; idx++)
        {   pin(dat_pin[idx], up);
        }
    }
    bool read_bit(int idx)
    {   return digitalRead(dat_pin[idx]);
    }

    struct co_send
    {   co_delay delay;
        int i;

        COROUTINE(knob & m, byte data)
        {   for(i=7 ; i>=0 ; i--)
            {   m.clk(0);
                m.write_all(data&(1<<i));
                AWAIT(delay(dt));
                m.clk(1);
                AWAIT(delay(dt));
            }
            m.clk(0);
            m.write_all(1);
            AWAIT(delay(dt));
            m.clk(1);
            AWAIT(delay(dt));
        //        for(int j=0 ; j<devs ; j++)
        //        {   bool ack = (read_bit(j) == ACK);
        //            Serial.printf("%d: %s\n", j, ack?"ack":"nack");
        //        }
            COFINISH();
        }
    };

    struct co_start
    {   co_delay delay;
        co_send send;
        int i;
        COROUTINE(knob & m, byte addr, bool mode)
        {   m.clk(1);
            AWAIT(delay(dt));
            m.write_all(0);
            AWAIT(delay(dt));
            AWAIT(send(m, (addr<<1)|mode));
            COFINISH();
        }
    };

    struct co_recv
    {   co_delay delay;
        int i;

        COROUTINE(knob & m, byte * bytes, int devpitch)
        {   for(int j=0 ; j<devs ; j++)
            {   bytes[j * devpitch] = 0;
            }
            // byte bits big endian
            for(i=7 ; i>=0 ; i--)
            {   m.clk(0);
                m.write_all(1);
                AWAIT(delay(dt));
                m.clk(1);
                AWAIT(delay(dt));
                for(int j=0 ; j<devs ; j++)
                {   bytes[j * devpitch] |= (m.read_bit(j)<<i);
                }
            }
            m.clk(0);
            m.write_all(ACK);
            AWAIT(delay(dt));
            m.clk(1);
            AWAIT(delay(dt));
            COFINISH();
        }
    };

    struct co_stop
    {   co_delay delay;
        int i;

        COROUTINE(knob & m)
        {   // stop
            m.clk(0);
            m.write_all(0);
            AWAIT(delay(dt));
            m.clk(1);
            AWAIT(delay(dt));
            m.write_all(1);
            AWAIT(delay(dt));
            COFINISH();
        }
    };

    struct co_send_bytes
    {   co_start start;
        co_send send;
        co_stop stop;
        int i;

        COROUTINE(knob & m, byte dev_addr, byte const* bytes, int nbytes)
        {   AWAIT(start(m, dev_addr, WRITE));
            for(i=0 ; i<nbytes ; i++)
            {   AWAIT(send(m, bytes[i]));
            }
            AWAIT(stop(m));
            COFINISH();
        }
    };

    struct co_recv_bytes
    {   co_start start;
        co_recv recv;
        co_stop stop;
        int i;
        
        // byte bytes[ndevs][nbytes]
        COROUTINE(knob & m, byte dev_addr, byte * bytes, int nbytes)
        {   AWAIT(start(m, dev_addr, READ));
            for(i=0 ; i<nbytes ; i++)
            {   AWAIT(recv(m, bytes+i, nbytes));
            }
            AWAIT(stop(m));
            COFINISH();
        }
    };

    co_delay delay;
    co_send_bytes send;
    co_recv_bytes recv;
    byte data[devs][readlen];
    
    static constexpr uint8_t avgn = 32;
    static constexpr uint8_t mindelta = 32;
    byte i2c_hall_init[2][2] = {
        {0x10, 0xa9}, // x-y-z enable, adc after read, x2 sens, parity
        {0x11, 0x15} // 1 byte read, no INT, master control mode
    };
    struct hall_state
    {   int16_t x[avgn] = {};
        int16_t y[avgn] = {};
        int32_t avgx = 0;
        int32_t avgy = 0;
        uint16_t angle = 0;
        bool init = false;
    };
    hall_state halls[devs] = {};
    // int16_t x[devs][avgn] = {};
    // int16_t y[devs][avgn] = {};
    // int32_t avgx[devs] = {};
    // int32_t avgy[devs] = {};
    // uint16_t angle[devs] = {};
    // bool init[devs] = {};
    uint8_t seq = 0;

    uint16_t atan16(int32_t y, int32_t x)
    {   if(y < 0) { return 0 - atan16(-y, x); }
        if(x < 0) { return 32768 - atan16(y, -x); }
        if(y > x) { return 16384 - atan16(x, y); }
        float a = float(y)/float(x);
        return a * (8192 - (a-1) * (2548.6 + 697.23 * a));
    }

    COROUTINE()
    {   AWAIT(delay(100000));
        // sensor reset sequence
        AWAIT(send(*this, 0xff, nullptr, 0));
        AWAIT(send(*this, 0xff, nullptr, 0));
        AWAIT(send(*this, 0x00, nullptr, 0));
        AWAIT(send(*this, 0x00, nullptr, 0));
        AWAIT(delay(40));
        // configure sensers
        AWAIT(send(*this, dev_addr, i2c_hall_init[0], 2));
        AWAIT(send(*this, dev_addr, i2c_hall_init[1], 2));
        AWAIT(delay(30000));
        
        while(1)
        {
            AWAIT(delay(300));
            AWAIT(recv(*this, dev_addr, (byte*)data, readlen));
            seq = (seq + 1) % avgn;
            for(int id=0 ; id<devs ; id++)
            {
                hall_state & dev = halls[id];
                int16_t ix = (int8_t)data[id][0];
                int16_t iy = (int8_t)data[id][1];
                // int16_t iz = (int8_t)data[id][2];
                ix = (ix<<4) | (data[id][4] >> 4);
                iy = (iy<<4) | (data[id][4] & 0xf);
                // iz = (iz<<4) | (data[id][5] & 0xf);
                dev.avgx -= dev.x[seq];
                dev.avgx += (dev.x[seq] = ix);
                dev.avgy -= dev.y[seq];
                dev.avgy += (dev.y[seq] = iy);

                // send updates less often
                if(seq % 8 == 0)
                {   uint16_t ang = atan16(dev.avgy, dev.avgx);
                    int16_t delta = ang - dev.angle;
                    if(abs(delta) > mindelta)
                    {   if(dev.init) { delegate.control(id, delta); }
                        dev.angle = ang;
                        dev.init = true;
                    }
                }
            }
        }
        COFINISH();
    }
};

} // teensy
