#pragma once

#include "coroutine.h"

namespace teensy {

template<class Delegate, int key_n, int drive_n, int sense_n>
struct key
{
    Delegate & delegate;
    byte const (&drive_pin)[drive_n];
    byte const (&sense_pin)[sense_n];
    int8_t const (&key_map)[drive_n][sense_n];
    int button_offset;

    struct State
    {   uint32_t sw1_time = 0;
        byte state = 0;
        byte channel = 255;
        byte note = 255;
        byte sw_n = 0;
    };

    State keys[key_n] = {};
    byte midi_offset = 36;
    byte midi_channel = 0;
    
    void setup()
    {   for(int i=0 ; i<sense_n ; i++)
        {   pinMode(sense_pin[i], INPUT_PULLUP);
        }
        for(int i=0 ; i<drive_n ; i++)
        {   pinMode(drive_pin[i], OUTPUT);
            digitalWrite(drive_pin[i], 1); // active low
            for(int j=0 ; j<sense_n ; j++)
            {   if(key_map[i][j] >= 0) { keys[key_map[i][j]].sw_n ++; }
            }
        }
    }

    // Debounce buttons
    void button_down(int k)
    {   delegate.button_down(k-button_offset);
    }
    void button_up(int k)
    {   delegate.button_up(k-button_offset);
    }
    void update_button(int k, byte switches)
    {   byte & st = keys[k].state;
        if(st != switches)
        {   uint32_t now = micros();
            // 50ms min time between state changes
            if(now - keys[k].sw1_time > 50000)
            {   keys[k].sw1_time = now;
                st = switches;
                switches ? button_down(k) : button_up(k);
            }
        }
    }

    // Measure 2-switch key velocity
    void down(int k)
    {   if(k >= button_offset)
        {   delegate.button_down(k-button_offset);
            return;
        }
        uint32_t dt = micros() - keys[k].sw1_time;
        if(dt > 100000) { dt = 100000; }
        if(dt < 2000) { dt = 2000; }
        byte vel = 254000 / dt;
        keys[k].note = midi_offset + k;
        keys[k].channel = midi_channel;
        delegate.key_down(keys[k].channel, keys[k].note, vel);
    }
    void up(int k)
    {   if(k >= button_offset)
        {   delegate.button_up(k-button_offset);
            return;
        }
        delegate.key_up(keys[k].channel, keys[k].note);
    }
    void update(int k, byte switches)
    {   byte & st = keys[k].state;
        switch(st)
        {   case 0:
                if(switches > 0) { keys[k].sw1_time = micros(); st = 1; }
                break;
            case 1:
                if(switches == keys[k].sw_n) { down(k); st = 2; }
                if(switches < 1) { st = 0; }
                break;
            case 2:
                if(switches < 2) { st = 3; }
                break;
            default: // 3
                if(switches < 1) { up(k); st = 0; }
                break;
        }
    }

    co_delay _delay_us;
    int d; // must be here for AWAIT to work
    byte switches[key_n] = {0};
    
    COROUTINE()
    {
        setup();
        while(1)
        {   // drive 1 drive pin at a time
            for(d=0 ; d<drive_n ; d++)
            {   // active low
                digitalWrite(drive_pin[d], LOW);
                
                // magic trick to leave this function and resume after 10 micros
                AWAIT(_delay_us(10));
                
                for(int i=0 ; i<sense_n ; i++)
                {   int k = key_map[d][i];
                    if(k<0) { continue; }
                    // count switches pressed for each key
                    switches[k] += !digitalRead(sense_pin[i]);
                }
                digitalWrite(drive_pin[d], HIGH);
            }
            // react to and reset switch counts
            for(int k=0 ; k<button_offset ; k++)
            {   update(k, switches[k]);
                switches[k] = 0;
            }
            for(int k=button_offset ; k<key_n ; k++)
            {   update_button(k, switches[k]);
                switches[k] = 0;
            }
        }
        COFINISH();
    }
};

} // teensy