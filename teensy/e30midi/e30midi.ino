#include "teensy/audio.h"
#include "teensy/button.h"
#include "teensy/led.h"
#include "teensy/key.h"
#include "teensy/knob.h"
#include "teensy/power.h"

auto delay_ms = delay;
auto delay_us = delayMicroseconds;

// knobs
constexpr int nknobs = 7;
constexpr byte knob_clock_pin = 34;
constexpr byte knob_dat_pin[nknobs] = {35, 36, 37, 38, 39, 40, 41};

// keys
constexpr int nkeys = 49;
constexpr int nkey_drive = 8;
constexpr int nkey_sense = 12;
constexpr int key_button_offset = 37;
constexpr byte key_drive_pins[nkey_drive] = {6, 5, 4, 3, 2, 1, 0, 16};
constexpr byte key_sense_pins[nkey_sense] = {32, 31, 30, 29, 28, 27, 26, 25, 24, 12, 11, 9};
constexpr int8_t key_map[nkey_drive][nkey_sense] = {
    {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0, 0},
    { 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6},
    { 7, 7, 8, 8, 9, 9,10,10,11,11,12,12},
    {13,13,14,14,15,15,16,16,17,17,18,18},
    {19,19,20,20,21,21,22,22,23,23,24,24},
    {25,25,26,26,27,27,28,28,29,29,30,30},
    {31,31,32,32,33,33,34,34,35,35,36,36},
    {37,38,39,40,41,42,43,44,45,46,47,48},
};

// leds
constexpr int nleds = 20;
constexpr int nled_pins = 1;
constexpr uint8_t led_pins[nled_pins] = {33};

// power gpio pin - tied to on-off pin
constexpr int power_pin = 14;

// buttons (part of keys)

constexpr byte btn_bank5 = 0;
constexpr byte btn_bank0 = 5;
constexpr byte btn_track_rec = 6;
constexpr byte btn_track_play = 7;
constexpr byte btn_track_down = 8;
constexpr byte btn_track_up = 9;
constexpr byte btn_octave_down = 10;
constexpr byte btn_octave_up = 11;

constexpr byte midi_chan = 1;

struct midi_manager
{
    teensy::button<midi_manager> _extra_button { *this, power_pin };
    
    teensy::key<midi_manager, nkeys, nkey_drive, nkey_sense> _keys { 
        *this, key_drive_pins, key_sense_pins, key_map, key_button_offset };
    
    teensy::knob<midi_manager, nknobs> _knobs { 
        *this, knob_clock_pin, knob_dat_pin };
    
    teensy::power<midi_manager> _power { *this, power_pin };
    
    teensy::led _leds { nled_pins, nleds, led_pins };
   
    float _controls[7];

    void button_down(byte id)
    {   usbMIDI.sendControlChange(id, 127, midi_chan);
    }
    void button_up(byte id)
    {   usbMIDI.sendControlChange(id, 0, midi_chan);
    }
    void update_button(int pin, bool state)
    {   usbMIDI.sendControlChange(12, state ? 0 : 127, midi_chan);
    }
    void key_down(byte chan, byte id, byte vel)
    {   usbMIDI.sendNoteOn(id, vel, midi_chan);
//        Serial.printf("key %d down %d\n", id, vel);
    }
    void key_up(byte chan, byte id, byte vel=0)
    {   usbMIDI.sendNoteOff(id, vel, midi_chan);
//        Serial.printf("key %d up %d\n", id, vel);
    }
    void control(byte id, int16_t delta)
    {   
        float dx = delta / float(100000);
//        dx *= (1 + std::abs(dx) * 10);
        _controls[id] = max(0, min(_controls[id] - dx, 1));

        uint16_t c14 = _controls[id] * (0x3fff);
        byte l7 = c14 & 0x7f;
        byte u7 = c14 >> 7;

//        Serial.printf("knob %d at %d\n", id, c14);
        
        usbMIDI.sendControlChange(20+id, u7, midi_chan);
        usbMIDI.sendControlChange(30+id, l7, midi_chan);
    }
    void midi_ctl(byte id, byte value)
    {   if(id < 20 || id >= 27) { return; }
        _controls[id - 20] = (value + 0.5f) / 128;
    }

    void refresh_knob_leds()
    {   for(int i=0 ; i<7 ; i++)
        {   int led = 6 + i*2;
            float val = _controls[i];
            int v = 1408 * val;
            int r = max(0, min(abs(v - 896) - 384, 255)) / 8;
            int g = max(0, min(384 - abs(v - 640), 255)) / 8;
            int b = max(0, min(384 - abs(v - 1152), 255)) / 8;
            _leds.set_color(led, (r<<16)|(g<<8)|(b));
        }
    }

    void refresh_bank_leds()
    {   
//        uint32_t pagecolors[] = {0x220000, 0x222200, 0x002200};
//        uint32_t mixercolor = 0x000022;
//        uint32_t oncolor = _chi == 6 ? mixercolor : pagecolors[_page];
//        for(int i=0 ; i<7 ; i++)
//        {   _leds.set_color(7+2*i, i==_chi ? oncolor : 0);
//        }
//
//        int loopstate = _ch[_keychi]->loopstate();
//        uint32_t offcolor = (loopstate&1) ? 0x000022 : 0x000000;
//        _leds.set_color(4, (loopstate&2) ? 0x004400 : offcolor);
//        _leds.set_color(5, (loopstate&4) ? 0x440000 : offcolor);
    }

    co_delay _delay_us;
    COROUTINE()
    {   while(1)
        {   AWAIT(_delay_us(10000)); // 100 Hz
            refresh_bank_leds();
            refresh_knob_leds();
        }
        COFINISH();
    }

    void setup()
    {   _leds.setup();
    }

    void loop()
    {   _power();
        _keys();
        _knobs();
        _leds();
        _extra_button();
        (*this)();
    }

    void pre_shutdown()
    {   Serial.println("Switching off now");
        Serial.flush();
        _leds.shutdown();
    }
};

midi_manager mgr;

void midi_ctl(byte channel, byte control, byte value)
{   mgr.midi_ctl(control, value);
}
void setup()
{   Serial.begin(15200);
    mgr.setup();
    usbMIDI.setHandleControlChange(midi_ctl);
}

void loop()
{   mgr.loop();
    delay_us(5);
}
