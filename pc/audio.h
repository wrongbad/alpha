#pragma once

#include "rtaudio/RtAudio.h"

namespace pc {

struct audio_ctx
{
    int const frames;
    int const channels;
    int const sample_rate;
    bool stop;
};

template<class Delegate>
class audio
{
public:
    static int render(void * out, void * in, unsigned frames,
            double /*time*/, RtAudioStreamStatus /*status*/, void * data)
    {
        audio & ths = *(audio*)data;
        audio_ctx ctx { int(frames), ths._channels, ths._sample_rate, false };
        ths._delegate.render((float const*)in, (float*)out, ctx);
        return (int)ctx.stop;
    }
    audio(Delegate & delegate)
    :   _delegate(delegate)
    {
    }
    void start(int channels = 2, int sample_rate = 44100, int buffer_size = 128)
    {   _sample_rate = sample_rate;
        _channels = channels;

        RtAudio::StreamParameters out_params;
        out_params.deviceId = _driver.getDefaultOutputDevice();
        out_params.nChannels = channels;
        out_params.firstChannel = 0;
        
        RtAudio::StreamOptions options;
        options.flags = RTAUDIO_SCHEDULE_REALTIME;
        // options.flags |= RTAUDIO_HOG_DEVICE;
        // options.flags |= RTAUDIO_NONINTERLEAVED;

        unsigned buffer_request = buffer_size;
        _driver.openStream(
            &out_params, 
            nullptr, // input params
            RTAUDIO_FLOAT32, 
            sample_rate,
            &buffer_request,
            &render,
            (void*)this,
            &options,
            nullptr // err callback
        );
        _driver.startStream();
    }
    void stop()
    {   _driver.stopStream();
        _driver.closeStream();
    }
    int rate() const
    {   return _sample_rate;
    }
    int channels() const
    {   return _channels;
    }
private:
    Delegate & _delegate;
    int _sample_rate;
    int _channels;
    RtAudio _driver;
};

} // pc