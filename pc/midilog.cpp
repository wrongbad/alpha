#include "midi.h"

#include <iostream>
#include <chrono>
#include <thread>

int main(void)
{
    std::string cmds[16] = {
        "","","","","","","","",
        "NoteOff ", "NoteOn  ", "Pressure", "Control ",
        "Program ", "Pressure", "Pitch   ", "System  "
    };

    pc::midi midi;

    if(midi.input_count() == 0)
    {   std::cerr << "no midi sources found" << std::endl;
        return 1;
    }
    for(int i=0 ; i<midi.input_count() ; i++)
    {   std::cout << i << " : " << midi.input_name(i) << std::endl;
    }
    
    int which = 0;
    if(midi.input_count() > 1)
    {   std::cout << "choose input number : " << std::flush;
        std::cin >> which;
    }
    
    midi.connect_input(which);
    std::vector<uint8_t> msg;
    while(1)
    {   
        while(midi.receive(msg))
        {
            std::cout << "ch" << int(msg[0] & 0x0f)
                << " " << cmds[msg[0] >> 4]
                << " " << int(msg[1]) << " " << int(msg[2]) << std::endl;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
}