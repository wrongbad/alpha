#include "synth/synth.h"

#include "pc/audio.h"
#include "pc/midi.h"

#include <chrono>
#include <thread>
#include <ios>

int main(void)
{
    constexpr size_t SampleRate = 44100;
    constexpr size_t BufferSize = 64;
    constexpr size_t Polyphony = 12;
    constexpr size_t Channels = 7;
    constexpr size_t Stereo = 2;
    using Synth = bad::Synth<Channels, Polyphony>;

    Synth synth { SampleRate };
    SimpleAudio audio { Stereo, SampleRate, BufferSize };
    SimpleMidiIn midi_in { "Mini MIDI" };
    SimpleMidiIn midi_in2 { "InControl" };
    // TODO
    // SimpleMidiOut midi_out { "InControl" };
    SimpleMidiOut midi_out { "Port 2" };

    std::vector<uint8_t> msg(16);
    audio.start([&] (float const* in, float* out, AudioContext& ctx) { 
        while(midi_in.get(msg))
        {   int cmd = msg[0] & 0xF0;
            int d1 = msg.size()>1 ? msg[1] : 0;
            int d2 = msg.size()>2 ? msg[2] : 0;
            // std::cout << std::hex << int(msg[0]) << std::endl;
            synth.rxMidi(0, cmd, d1, d2);
        }
        while(midi_in2.get(msg))
        {   int cmd = msg[0] & 0xF0;
            int d1 = msg.size()>1 ? msg[1] : 0;
            int d2 = msg.size()>2 ? msg[2] : 0;
            // std::cout << std::hex << d1 << std::endl;
            synth.rxMidi(1, cmd, d1, d2);
        }
        std::fill(out, out+ctx.frames, 0);
        synth.render(in, out, ctx.frames);
        
        // mono -> stereo
        for(int i=ctx.frames-1 ; i>=0 ; i--)
        {   out[i*2+1] = out[i*2] = out[i];
        }
        // for(int i=0 ; i<ctx.frames ; i++)
        // {   out[i+ctx.frames] = out[i];
        // }
    });
    synth.txMidi([&] (uint8_t cmd, uint8_t d1, uint8_t d2) {
        uint8_t tmp[3] = {cmd, d1, d2};
        midi_out.send(&tmp[0], 3);
    });

    while(1) // wait for ctrl-c
    {   std::this_thread::sleep_for(std::chrono::seconds(5));
    }
}