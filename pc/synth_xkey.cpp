#include "pc/audio.h"
#include "pc/midi.h"

#include "wade/simple_flute.h"
#include "wade/simple_pluck.h"
#include "wade/finnwave.h"
#include "wade/polyphony.h"
#include "wade/delay.h"
#include "wade/reverb.h"

#include <unistd.h> // sleep
#include <ios>


constexpr int sample_rate = 44100;
constexpr int buffer_size = 64;
constexpr int polyphony = 12;
constexpr int stereo = 2;

namespace wade {
float sample_rate() { return ::sample_rate; }
float tune_a4() { return 440; }
} // wade

struct synth_manager
{
    pc::audio<synth_manager> _audio { *this };

    wade::midi_parser<synth_manager> _midi_parse { *this };
    std::vector<uint8_t> _midi_msg = std::vector<uint8_t>(16);
    pc::midi _midi;

    // using Voice = wade::finnwave;
    // using Voice = wade::simple_flute;

    wade::arr<float, 16> _control { 0.5 };
    // wade::arr<Voice, polyphony> _voices { _control.data() };
    wade::poly<wade::simple_flute, polyphony> _poly { _control.data() };

    // wade::tape_delay<65536> _delay;
    wade::reverb _reverb;

    // TODO 
    synth_manager()
    {   //_control[2] = 0; // inharm / detune
        _control[8] = 0; // delay send
    }
    void note_on(uint8_t note, uint8_t vel, uint8_t /*chan*/)
    {   _poly.note_on(note, vel);
    }
    void note_off(uint8_t note, uint8_t vel, uint8_t /*chan*/)
    {   _poly.note_off(note, vel);
    }
    void note_pressure(uint8_t note, uint8_t vel, uint8_t /*chan*/)
    {   _poly.note_pressure(note, vel);
    }
    void control(uint8_t id, uint8_t val, uint8_t /*chan*/)
    {   // _control[mid] = val / 127.0f;
    }
    void render(float const* /*inp*/, float * outp, pc::audio_ctx ctx)
    {
        while(_midi.receive(_midi_msg))
        {   _midi_parse(_midi_msg.data(), _midi_msg.size());
        }

        for(int i=0 ; i<ctx.frames ; i++)
        {   outp[i] = 0;
        }
        
        _poly.render(outp, ctx.frames);

        for(int i=0 ; i<ctx.frames ; i++)
        {   outp[i] = _reverb(outp[i]);
        }

        // mono -> stereo
        for(int i=ctx.frames-1 ; i>=0 ; i--)
        {   outp[i*2+1] = outp[i*2] = outp[i];
        }
    }
};

int main(int argc, char ** argv)
{
    std::string hint = "Xkey";
    if(argc >= 2) { hint = argv[1]; }

    synth_manager synth;
    synth._midi.connect_input(hint);
    synth._audio.start(stereo, sample_rate, buffer_size);
    while(1) { sleep(5); } // wait for ctrl-c
}