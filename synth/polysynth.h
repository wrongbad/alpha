#pragma once

#include "util/ring.h"

#include <algorithm> // std::min

namespace bad {

template<class MonoSynth, int Voices>
class PolySynth
{
public:
    struct Note
    {   int chan;
        int note;
        int vel;
    };

    template<class... Args>
    PolySynth(Args const&... voiceArgs)
    :   _voiceStore(voiceArgs...),
        _notes(Note{-1, 0})
    {
        for(int i=0 ; i<Voices ; i++)
        {   _voices[i] = &_voiceStore[i];
        }
    }
    void noteOn(int chan, int note, int vel)
    {   // push_front operation
        _notes.shift(1);
        _notes[0] = {chan, note, vel};
        _voices.shift(1);
        _voices[0]->noteOn(note, vel);
        _count ++;
    }
    void noteOff(int chan, int note, int vel = 0)
    {   for(int i=_count-1 ; i>=0 ; i--)
        {   if(_notes[i].chan == chan && _notes[i].note == note)
            {   _notes.bubble(i, _count-1);
                _count --;
                if(i < Voices)
                {   int j = std::min(_count, Voices-1);
                    _voices.bubble(i, j);
                    if(j >= _count)
                    {   _voices[j]->noteOff(vel);
                    }
                    else
                    {   // restore note with veolocity 1
                        // since it wasn't actually restruck
                        _voices[j]->noteOn(_notes[j].note, 1);
                        // _voices[j]->noteOn(_notes[j].note, _notes[j].vel);
                    }
                }
                return;
            }
        }
    }
    void control(int chan, int id, float val)
    {   for(int i=0 ; i<_count && i<Voices ; i++)
        {   if(_notes[i].chan == chan)
            {   _voices[i].control(id, val);
            }
        }
    }
    void render(float const* /*in*/, float * out, int samples)
    {   for(int i=0 ; i<Voices ; i++)
        {   _voices[i]->render(out, samples);
        }
    }
private:
    Ring<MonoSynth, Voices> _voiceStore;
    // Track voices and notes in order most recent first
    Ring<MonoSynth*, Voices> _voices;
    Ring<Note, 128> _notes;
    int _count = 0;
};

} // bad
