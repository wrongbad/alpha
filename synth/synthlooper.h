#pragma once

#include "synth/controlstate.h"
#include "synth/notestate.h"

#include "util/range.h"
#include "util/midi.h"
#include "util/err.h"

#include <array>
#include <vector>
#include <iostream> // cout

namespace bad {

class Loop
{
public:
    struct Event
    {   double delay;
        int cmd;
        int d1;
        float d2;
    };
    void restart()
    {   _delay = 0;
        _head = 0;
        _controlState.reset();
        _noteState.reset();
    }
    void reset()
    {   restart();
        _length = 0;
        _size = 0;
    }
    void event(int cmd, int d1 = 0, float d2 = 0)
    {   _size + 1 < _loop.size() || err("loop overflow");
        _loop[_size++] = Event{_delay, cmd, d1, d2};
        _delay = 0;
    }
    void noteOn(int chan, int note, float vel)
    {   _noteState.noteOn(chan, note, vel);
        event(midi::on, note, vel);
    }
    void noteOff(int chan, int note, float vel)
    {   _noteState.noteOff(chan, note, vel);
        event(midi::off, note, vel);
    }
    void controlRel(int id, float delta)
    {   _controlState.controlRel(id, delta);
        event(midi::ctl, id, delta);
    }
    void recStep(int dt)
    {   _delay += dt;
        _length += dt;
    }
    void recStop(double adjust)
    {   while(_delay + adjust < 0 && _size > 0)
        {   Event const& e = _loop[_size-1];
            if(e.cmd == midi::off) { _noteState.noteOn(1, e.d1, 1); }
            if(e.cmd == midi::on) { _noteState.noteOff(1, e.d1, 1); }
            if(e.cmd == midi::ctl) { _controlState.controlRel(e.d1, -e.d2); }
            _delay += e.delay;
            _size--;
        }
        _delay += adjust;
        _length += adjust;

        _noteState.desync(*this);
        _controlState.desync(*this);
        if(_delay > 0) { event(0); }

        // printLoop();

        restart();
    }
    template<class DestBox>
    void playStep(DestBox & dest, int dt)
    {   _delay += dt;
        if(_size == 0 || _delay < _loop[_head].delay) { return; }
        _delay -= _loop[_head].delay;
        Event const& e = _loop[_head];
        _head = (_head + 1) % _size;
        if(e.cmd == midi::off)
        {   dest.noteOff(1, e.d1, e.d2);
            _noteState.noteOff(1, e.d1, e.d2);
        }
        if(e.cmd == midi::on)
        {   dest.noteOn(1, e.d1, e.d2);
            _noteState.noteOn(1, e.d1, e.d2);
        }
        if(e.cmd == midi::ctl)
        {   dest.controlRel(e.d1, e.d2);
            _controlState.controlRel(e.d1, e.d2);
        }
    }
    template<class DestBox>
    void playStop(DestBox & dest)
    {   _noteState.desync(dest);
        _controlState.desync(dest);
        restart();
    }
    double length() const { return _length; }

    void printLoop() const
    {
        for(int i=0 ; i<_size ; i++)
        {   Event const& e = _loop[i];
            std::cout << "d" << e.delay
                << " c" << e.cmd
                << " " << e.d1
                << " " << e.d2 << "\n";
        }
        std::cout << "\n";
    }
private:
    NoteState _noteState;
    ControlState _controlState;
    // std::array<Event, 65536> _loop;
    std::vector<Event> _loop = std::vector<Event>(65536);
    double _length = 0;
    double _delay = 0;
    int _head = 0;
    int _size = 0;
};




template<class SynthCore>
class SynthLooper
{
public:
    SynthLooper(float sampleRate)
    :   _sampleRate(sampleRate)
    {
        control(7, 0.5); // 50% volume
        control(9, 1); // unmute

        for(int i=0 ; i<8 ; i++)
        {   control(21+i, 0.5); // neutral parameters
        }
    }
    void rxMidi(int cmd, int d1 = 0, int d2 = 0)
    {   if(cmd == midi::on && d2 == 0) { cmd = midi::off; }
        switch(cmd)
        {   case midi::off: noteOff(0, d1, d2/127.0); break;
            case midi::on: noteOn(0, d1, d2/127.0); break;
            case midi::atouch: break; // d1=note d2=pressure

            // TODO!!!! switch this dynamically
            case midi::ctl: control(d1, d2/127.0); break;
            // case midi::ctl: controlRel(d1, (d2-64)/1500.0f); break;
            default: break;
        }
    }
    void noteOn(int chan, int note, float vel)
    {   _synth.noteOn(chan, note, vel);
        _noteState.noteOn(chan, note, vel);
        if(_rec) { _recLoop->noteOn(chan, note, vel); }
    }
    void noteOff(int chan, int note, int vel)
    {   _synth.noteOff(chan, note, vel);
        _noteState.noteOff(chan, note, vel);
        if(_rec) { _recLoop->noteOff(chan, note, vel); }
    }
    void control(int id, float val)
    {   float delta = val - _midiControls[id];
        _midiControls[id] = val;
        controlRel(id, delta);
    }
    void controlRel(int id, float delta)
    {   _controlState.controlRel(id, delta);
        _clampedControls[id] = _controlState.clamped(id);
        // std::cout << id << ": " << _clampedControls[id] << "\n";
        if(_rec) { _recLoop->controlRel(id, delta); }
    }
    void render(float const* in, float * out, int samples)
    {   if(_play) { _playLoop->playStep(*this, samples); }
        if(_rec) { _recLoop->recStep(samples); }
        _synth.render(in, out, samples);
    }
    bool record(SynthLooper * all, int channels)
    {   _rec = !_rec;
        if(_rec)
        {   _recLoop->reset();
            _noteState.sync(*_recLoop);
        }
        else
        {   double shortest = 6e6;
            double adjust = 0;
            double thislen = _recLoop->length();
            for(SynthLooper & l : range(all, channels))
            {   if(&l != this && l.length() > 0 && l.length() < shortest)
                {   shortest = l.length();
                }
            }
            if(shortest < 6e6)
            {   double bestAdjust = 0.2 * _sampleRate;
                int mthis = 1, mlong = 1;
                while(mthis==1 || mlong==1)
                {   double d = shortest * mlong / mthis - thislen;
                    if(std::abs(d) < bestAdjust)
                    {   bestAdjust = std::abs(d);
                        adjust = d;
                    }
                    if(d > 0) { mthis++; }
                    else { mlong++; }
                }
            }
            _recLoop->recStop(adjust);
            _playLoop->playStop(*this);
            std::swap(_recLoop, _playLoop);
            if(!_play) { play(); }
        }
        return _rec;
    }
    bool recording() const { return _rec; }
    void cancel_record()
    {   if(!_rec) { return; }
        _recLoop->reset();
        _rec = false;
    }

    bool play()
    {   _play = !_play;
        if(!_play)
        {   _playLoop->playStop(*this);
        }
        return _play;
    }
    bool playing() const { return length() > 0; }
    double length() const { return _play ? _playLoop->length() : 0; }
    
    // TODO mute play loop not synth!
    bool toggle_mute()
    {   _mute = !_mute;
        control(9, _mute ? 0 : 1);
        return _mute;
    }
    bool muted() const { return _mute; }

    void unfocus()
    {   cancel_record();
    }

private:
    float const _sampleRate;

    NoteState _noteState;
    ControlState _midiControls;
    ControlState _controlState;
    ControlState _clampedControls;

    SynthCore _synth { _controlState, _sampleRate };

    bool _rec = false;
    bool _play = true;
    bool _mute = false;

    Loop _loops[2];
    Loop * _playLoop = &_loops[0];
    Loop * _recLoop = &_loops[1]; 
};

} // bad
