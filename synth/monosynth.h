#pragma once

#include "synth/controlstate.h"

#include "util/ring.h"
#include "util/midi.h"

namespace bad {

class MonoSynth
{
public:
    MonoSynth(ControlState const& controls, float sampleRate)
    :   _controls(controls),
        _srate(sampleRate)
    {
        // for(int i=0 ; i<128 ; i++)
        // {   _noteCtl[i] = 0.5;
        // }
    }
    void noteOn(int key, int vel)
    {   _key = key;
        _freq = midi::freq(key);
        _gate = 1;
        _phase = 0;
        _phase2 = 0;
        _time = 0;
        _attacking = true;
    }
    void noteOff(int vel = 0)
    {   _gate = 0;
        _attacking = false;
    }
    // Note-specific controls (MPE)
    // void control(int id, float val)
    // {   _noteCtl[id] = val;
    // }
    void render(float * out, int samples)
    {   
        using std::min; using std::max;
        using std::sin; using std::cos;
        using std::exp; using std::tanh;
        
        // volume + mute handling
        float volume = _controls[7] * _controls[9] * 0.03;
        // bass boost
        volume *= (1 - 0.5 * tanh((_freq - 150) / 100));

        // softer than low-pass cutoff
        float lp_mushoff = exp(_controls[21] * -4 + _key * 0.02);
        // negate even harmonics
        float nevens = _controls[22];
        // detune even harmonics
        float det = _controls[23] * 0.03;

        float attack = exp(max(_controls[24]-0.5f, 0.0f) * 20) + 50;
        float decay = exp(min(_controls[24], 0.6f) * 30 + 5) + 50;
        float release = exp(_controls[25] * 13) + 50;

        // harmonics below nyquist
        int n = _srate / _freq / 2;

        float p = _phase * 2 * M_PI;
        float p2 = _phase2 * 2 * M_PI;
        float dp = _freq / _srate * 2 * M_PI;
        float dp2 = _freq / _srate * 2 * M_PI * (2+det); 
        float dt = 1.0 / _srate;
        for(int i=0 ; i<samples ; i++)
        {   
            _time += dt;
            p += dp;
            p2 += dp2;

            if(_attacking) { _env += max(_gate - _env, 0.0f) / attack; }
            else if(decay < _srate*10) { _env += min(0 - _env, 0.0f) / decay; }
            _env += max((_gate - 1)/release, -_env);

            if(_env > 0.95) { _attacking = false; }
            
            if(volume == 0 || _env == 0) { continue; }

            // float et = _time * 0.5 + 1;
            // float b = lp_mushoff * et;

            float b = lp_mushoff / (_env + 0.001);
            float a = b * 10 * _env;

            float y = a * (
                exp(-b) * sin(p)
                - exp(-(n+1)*b) * sin((n+1)*p)
                + exp(-(n+2)*b) * sin(n*p)
            ) / ( 1 - 2*exp(-b)*cos(p) + exp(-2*b) );

            float b2 = b * 2;
            float a2 = a * exp(-b) * nevens;
        
            float y2 = a2 * (
                exp(-b2) * sin(p2)
                - exp(-(n+1)*b2) * sin((n+1)*p2)
                + exp(-(n+2)*b2) * sin(n*p2)
            ) / ( 1 - 2*exp(-b2)*cos(p2) + exp(-2*b2) );
            
            out[i] += (y - y2) * volume;
        }
        p = p * (0.5 / M_PI);
        p2 = p2 * (0.5 / M_PI);
        _phase = p - std::floor(p);
        _phase2 = p2 - std::floor(p2);
    }
private:
    ControlState const& _controls;
    float const _srate;

    // float _noteCtl[128];

    int _key = 0;
    double _time = 0;
    bool _attacking = false;
    float _freq = 0;
    float _gate = 0;
    float _env = 0;
    float _phase = 0;
    float _phase2 = 0;
};

} // bad
