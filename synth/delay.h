#pragma once

#include "dsp/va_filter.h"

#include <stdexcept>

namespace bad {

class Delay
{
public:
    Delay(ControlState const& controls, int max_size)
    :   _controls(controls),
        _buf(max_size)
    {}
    static float next(float x)
    {   float y = std::ceil(x);
        return y==x ? y+1 : y;
    }
    static float prev(float x)
    {   float y = std::floor(x);
        return y==x ? y-1 : y;
    }
    void render(float * audio, int frames)
    {   int s = _buf.size();
        float feedback = std::min(_controls[21], 0.999f);
        float delay_in = _controls[22] * s;

        if(_delay_in < 0)
        {   _delay_in = delay_in;
            _delay = _delay_in + 1;
        }


        for(int i=0 ; i<frames ; i++)
        {   
            _delay_in += (delay_in - _delay_in) * 0.0001;
            
            float tot_dt = std::abs(_delay_in - _delay);
            // float tot_dt = 1;
            // if(tot_dt < 0.01) { throw std::runtime_error("poop"); }

            // if(std::abs(tot_dt) < 0.01) { std::cout << "tot " << tot_dt << std::endl; }

            float fb = 0;
            float target = next(_delay);
            while(target < _delay_in)
            {   float dt = target - _delay;

                if(dt > tot_dt) { std::cout << "dt1 " << dt 
                        << "tot " << tot_dt 
                        << " _delay " << double(_delay)
                        << " _delay_in " << double(_delay_in)
                        << " target " << double(target)
                        << std::endl; }

                fb = _filter.step(_buf[(_bufi + int(_delay)) % s], dt / tot_dt);
                _delay = target;
                target ++;
            }
            target = prev(_delay);
            while(target > _delay_in)
            {   float dt = _delay - target;
                
                if(dt > tot_dt) { std::cout << "dt2 " << dt 
                        << " tot " << tot_dt 
                        << " _delay " << double(_delay)
                        << " _delay_in " << double(_delay_in)
                        << " target " << double(target)
                        << std::endl; }

                fb = _filter.step(_buf[(_bufi + int(target)) % s], dt / tot_dt);
                _delay = target;
                target --;
            }
            float dt = std::abs(_delay_in - _delay);
                
            if(dt > tot_dt) { std::cout << "dt3 " << dt 
                    << " tot " << tot_dt 
                        << " _delay " << double(_delay)
                        << " _delay_in " << double(_delay_in)
                    << std::endl; }

            fb = _filter.step(_buf[(_bufi + int(_delay_in)) % s], dt / tot_dt);
            _delay = _delay_in;

            if(std::abs(fb) > 10) { std::cout << "fb " << fb << std::endl; }

            audio[i] += fb * feedback;
            _buf[_bufi] = audio[i];
            _bufi = (_bufi + s - 1) % s;
            _delay ++; // keep filter aligned
        }
    }
private:
    ControlState const& _controls;

    std::vector<float> _buf;
    int _bufi = 0;

    va::filter<> _filter = va::chebychev1<>();
    float _delay = -1;
    float _delay_in = -1;
};

} // bad