#pragma once

#include "synth/polysynth.h"
#include "synth/monosynth2.h"
#include "synth/synthlooper.h"
#include "synth/delay.h"

#include "util/arr.h"
#include "util/intmod.h"

#include <functional>
#include <cassert>

namespace bad {

// TODO rename Channels because confusing with midi
template<int Channels, int Polyphony>
class Synth
{
public:
    using SynthCore = PolySynth<MonoSynth, Polyphony>;
    using Looper = SynthLooper<SynthCore>;
    using MidiSender = std::function<void(int,int,int)>;
    static constexpr int Mix = Channels-1;

    // LaunchKey Mappings
    // 96 - 104 (top row)
    // 112 - 120 (bottom row)
    static constexpr int midi_red = 0x03;
    static constexpr int midi_yellow = 0x33;
    static constexpr int midi_green = 0x30;
    static constexpr int midi_chan_ctl = 96; // +0 - +7
    static constexpr int midi_play = 104;
    static constexpr int midi_rec = 120;
    static constexpr int midi_knob = 21; // +0 - +7

    static constexpr int master_mix = 1;
    static constexpr int master_fx = 2;

    Synth(float sampleRate)
    :   _channels(sampleRate),
        _delay(_global_controls, 1<<15)
    {
        _global_controls[21] = 0.5;
        _global_controls[22] = 0.5;
    }
    void txMidi(MidiSender cb)
    {   _sendMidi = std::move(cb);
        // LaunchKey "InControl" mode enables led control
        // and splits keys from button/knob midi channels
        _sendMidi(midi::on, 12, 127);
        // Refresh led display
        rxMidi(1,0,0,0);
    }
    void rxMidi(int chan, int cmd, int id, int val)
    {   
        if(cmd == midi::on && val == 0) { cmd = midi::off; }

        // keys are on midi channel 0
        if(chan == 0)
        {   int sendto = _channel;
            if(cmd == midi::on) { _note_channel[id] = sendto; }
            if(cmd == midi::off) { sendto = _note_channel[id]; }
            _channels[sendto].rxMidi(cmd, id, val);
        }
        // toggle master overlays
        else if(cmd == midi::on && id == midi_chan_ctl + Channels)
        {   ++_master_bank;
            _channels[_channel].unfocus();
        }
        // select channel/bank
        else if(cmd == midi::on && id >= midi_chan_ctl && id < midi_chan_ctl+Channels)
        {   int rid = id - midi_chan_ctl;
            if(_master_bank == master_mix)
            {   _channels[rid].toggle_mute();
            }
            else
            {   set_channel(rid);
            }
        }
        
        Looper & c = _channels[_channel];
        if(cmd == midi::on && id == midi_rec)
        {   c.record(_channels.data(), Channels);
        }
        else if(cmd == midi::on && id == midi_play)
        {   c.play();
        }
        else if(cmd == midi::ctl && id >= midi_knob && id < midi_knob + Channels + 1)
        {   int rid = id - midi_knob;
            if(_master_bank == master_mix)
            {   if(rid < Channels)
                {   _channels[rid].rxMidi(cmd, 7, val);
                }
                else
                {   
                }
            }
            if(_master_bank == master_fx)
            {   _global_controls.control(id, val/127.0f);
            }
            else
            {   c.rxMidi(cmd, id, val);
            }
        }

        set_light(midi_rec, c.recording() ? midi_red : 0);
        set_light(midi_play, _master_bank || c.playing() ? midi_green : 0);

        for(int i=0 ; i<Channels ; i++)
        {   int col = 0;
            if(_master_bank == 0 && i == _channel)
            {   col = _chan_bank ? midi_red : midi_yellow;
            }
            if(_master_bank == 1 && !_channels[i].muted())
            {   col = midi_green;
            }
            set_light(i + midi_chan_ctl, col);
        }
        int col = 0;
        if(_master_bank)
        {   col = _master_bank==2 ? midi_red : midi_yellow;
        }
        set_light(midi_chan_ctl + Channels, col);
    }
    void render(float const* in, float * out, int frames)
    {   for(int i=0 ; i<Channels ; i++)
        {   _channels[i].render(in, out, frames);
        }
        _delay.render(out, frames);
        for(int i=0 ; i<frames ; i++)
        {   out[i] = std::max(-1.0f, std::min(out[i], 1.0f));
        }
    }
private:
    void set_light(int id, int color)
    {   if(_lights[id] == color) { return; }
        if(_sendMidi) { _sendMidi(color ? midi::on : midi::off, id, color); }
        _lights[id] = color;
    }
    void set_channel(int chan)
    {   if(_channel == chan) { ++_chan_bank; return; }
        _channels[_channel].unfocus();
        _channel = chan;
        _chan_bank = 0;
    }

    util::arr<Looper, Channels> _channels;
    int _channel = 0;
    util::intmod<2> _chan_bank = 0;
    util::intmod<3> _master_bank = 0;

    // release notes in channel they were pressed in
    int _note_channel[128];
    
    // remember light state to reduce midi flooding
    int _lights[128] {0};

    // generic send midi delegate
    MidiSender _sendMidi;

    ControlState _global_controls;

    Delay _delay;
};

} // bad
