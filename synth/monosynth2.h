#pragma once

#include "synth/controlstate.h"

#include "util/ring.h"
#include "util/midi.h"

#include <iostream>

namespace bad {

template<class T> int sgn(T val)
{   return (T(0) < val) - (val < T(0));
}

static constexpr int HMax = 128;

inline float frand()
{   return float(rand())/float(RAND_MAX);
}

class MonoSynth
{
public:
    MonoSynth(ControlState const& controls, float sampleRate)
    :   _controls(controls),
        _srate(sampleRate)
    {
        for(int h=1 ; h<HMax ; h++)
        {   _y[h] = _dy[h] = 0;
        }
        _time = 1e9;
    }
    void noteOn(int key, int vel)
    {   _key = key;
        _gate = 1;
        _time = 0;
        _dir = -_dir; // bow reversal

        // for(int h=1 ; h<HMax ; h++)
        // {   _y[h] = _dy[h] = 0;
        // }
        // _lp = 0;
        // _agc = 1;
    }
    void noteOff(int vel = 0)
    {   _gate = 0;
        _time = 0;
    }

    void render(float * out, int samples)
    {   
        using std::min; using std::max;
        using std::sin; using std::cos;
        using std::exp; using std::tanh;
        using std::abs; 
        
        // volume + mute handling
        float volume = _controls[7] * _controls[9];

        float dt = 1.0 / _srate;
        float lp_alpha = 5 / _srate;
        float pi = 3.1415926;

        float freq = midi::freq(_key);
        int n = min(int(_srate / freq / 2), HMax);

        // bass boost
        volume *= (1 - 0.5 * tanh((freq - 150) / 100));


        float bow_force = 1 * (exp(_controls[21] * 6) - 1);
        float bow_speed = .1 * (exp(_controls[22] * 6) - 1);
        float damp = .1 * exp(_controls[23] * 6);
        if(_gate) { damp = damp / 2; }
        float bow_x = max(0.01f, min(_controls[24]*0.55f, 0.99f)); 
        // float bow_force = .1;
        // float bow_speed = .1;
        // float damp = .0004;
        // float bow_x = 0.123; 
        // float pickup_x = 0.023;

        // static int print = 0;

        if(_freq != freq || _damp != damp)
        {   _freq = freq; _damp = damp;
            for(int h=1 ; h<HMax ; h++)
            {   float w = (2 * pi * _freq * h);
                float d = exp(-2 * damp * h * dt);
                _cos_dt[h] = cos(w * dt) * d;
                _sin_dt[h] = sin(w * dt) * d;
            }
        }

        if(_bow_x != bow_x)
        {   _bow_x = bow_x;
            for(int h=1 ; h<HMax ; h++)
            {   _sin_bow[h] = sin(pi * h * bow_x) * exp(-0.1 * h);
            }
        }

        // if(_pickup_x != pickup_x)
        // {   _pickup_x = pickup_x;
        //     for(int h=1 ; h<HMax ; h++)
        //     {   _sin_pickup[h] = sin(pi * h * pickup_x);
        //     }
        // }

        for(int i=0 ; i<samples ; i++)
        {   
            _gate2 += (_gate - _gate2) / (0.1 * _srate);

            // if(volume == 0 || _gate == 0) { break; }
            if(_gate < 1 && _time > _srate * 20) { break; }
            _time ++;

            // float bow_dy = bow_speed + frand() * 0.1;
            // float bow_dy = bow_speed;
            float bow_dy = 0;
            for(int h=1 ; h<n ; h++)
            {   bow_dy -= _dy[h] * _sin_bow[h] * h;
            }
            if(abs(bow_dy) < 1e-8 && !_gate) {
                // std::cout << "break\n";

                for(int h=1 ; h<HMax ; h++)
                {   _y[h] = _dy[h] = 0;
                }
                break;
            }

            bow_dy += _dir * bow_speed * _gate2;
            // bow_dy *= 55/_freq;
            // if(_clamped && _time % 100 == 1)
            // {
            //     std::cout << "clamped dy " << bow_dy << "\n"; 
            // }

            float force = bow_force * _gate2 * (0.3f + 1/(abs(bow_dy)*4+1)) * sgn(bow_dy);
            
            // float force = bow_force * _gate2 * (frand()*0.2 + 0.3f + 1/(abs(bow_dy)*4+1)) * sgn(bow_dy);

            // float force_prev = force;
            // force = max(force, -0.1f*abs(bow_dy) / dt);
            // force = min(force, 0.1f*abs(bow_dy) / dt);
            _force += 0.3 * (force - _force);

            // _force = force;

            // if(force != force_prev)
            // {
            //     std::cout << "clamped " << force_prev << " to " << force << "\n";
            //     std::cout << "freq " << _freq << "\n";
            // }
            // float force = bow_force * _gate * (0.3f + 1/(bow_dy*bow_dy*40+1)) * sgn(bow_dy);
                 

            // clamp force to that which makes bow_dy 0
            // without this, the "stick" of the bow sounds rattly and metalic
            // set next bow_dy to 0, and solving for force
            if(_gate)
            {
                float new_bow_dy = bow_speed;
                float denominator = 0;
                for(int h=1 ; h<n ; h++)
                {   float new_dy = _dy[h] * _cos_dt[h] - _y[h] * _sin_dt[h];
                    new_bow_dy -= new_dy * _sin_bow[h] * h;
                    denominator += _sin_dt[h] * _sin_bow[h] * _sin_bow[h] / h;
                }
                float force_limit = new_bow_dy / denominator;
                if(abs(_force) > abs(force_limit))
                {
                    // if(_time % 100 == 0)
                    // {
                    //     std::cout << "---\n";
                    //     std::cout << "clamped " << _force << " to " << force_limit << "\n";
                    //     std::cout << "bow_dy " << bow_dy << "\n";
                    //     std::cout << "new_bow_dy " << new_bow_dy << " den " << denominator << "\n";
                    // }
                    _force = force_limit;
                    _clamped = true;
                }
                else { _clamped = false; }
            }



            for(int h=1 ; h<n ; h++)
            {   // force is translated into a displacement of spring neutral
                // this is more numerically stable 
                float forceH = _sin_bow[h] * _force / h / h;
                float y = _y[h] - forceH;
                float dy = _dy[h];
                _y[h] = (y * _cos_dt[h] + dy * _sin_dt[h]) + forceH;
                _dy[h] = (dy * _cos_dt[h] - y * _sin_dt[h]);
            }

            float sample = 0;
            for(int h=1 ; h<n ; h++)
            {   
                // sample += _y[h] * _sin_pickup[h];
                sample += _y[h] * 0.1;
            }

            _lp += lp_alpha * (sample - _lp);
            sample -= _lp;

            if(_agc*abs(sample) > 0.5)
            {   
                // std::cout << "agc jump " << _agc << " to " << 0.5/abs(sample) << std::endl;
                // _agc = 0.5/abs(sample);
                _agc *= 0.99;

            }
            else
            {   _agc *= (1+dt);
                _agc = min(_agc, 2.0f);
            }
            sample *= _agc;
            // if(
            //     // (sample != 0 && ++print % 1000000 == 0) || 
            //     !(sample > -1 && sample < 1))
            // {
            //     std::cout << "sample=" << sample << "\n";
            //     std::cout << "agc=" << _agc << "\n";
            //     std::cout << "force=" << force << "\n";
            //     std::cout << "bow_dy=" << bow_dy << "\n";
            //     std::cout << "damp=" << _damp << "\n";
            // }
            // assert(sample >= -1 && sample <= 1);
            // if(!(sample >= -1 && sample <= 1)) { _gate = 0; break; }

            if(!(sample >= -1 && sample <= 1))
            {
                sample = sample > 0 ? 1 : -1;
            }
            // if(abs(_sample - sample) > 0.1)
            // {
            //     std::cout << "sample=" << sample << "\n";
            //     std::cout << "_sample=" << _sample << "\n";

            // }
            _sample = sample;
            
            out[i] += volume * sample;
        }
    }
private:
    ControlState const& _controls;
    float const _srate;

    int _key = 0;
    int _gate = 0;
    int _time = 0;
    int _dir = 1;

    float _freq = 0;
    float _damp = 0;
    float _gate2 = 0;

    float _sample = 0;

    bool _clamped = false;

    double _y[HMax];
    double _dy[HMax];
    double _lp = 0;

    float _force = 0;

    double _sin_dt[HMax];
    double _cos_dt[HMax];

    float _sin_bow[HMax];
    float _bow_x = 0;

    // float _sin_pickup[HMax];
    // float _pickup_x = 0;

    float _agc = 1;
};

} // bad
