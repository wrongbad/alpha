#pragma once

namespace bad {

class ControlState
{
public:
    void reset()
    {   for(int i=0 ; i<128 ; i++)
        {   _ctl[i] = 0;
        }
    }
    float const& operator[](int id) const
    {   return _ctl[id];
    }
    float & operator[](int id)
    {   return _ctl[id];
    }
    float clamped(int id) const
    {   return std::max(0.0f, std::min(_ctl[id], 1.0f));
    }
    void control(int id, float val)
    {   _ctl[id] = val;
    }
    void controlRel(int id, float delta)
    {   _ctl[id] += delta;
    }
    template<class Dest>
    void sync(Dest & dest)
    {   for(int i=0 ; i<128 ; i++)
        {   if(_ctl[i] != 0)
            {   dest.controlRel(i, _ctl[i]);
            }
        }
    }
    template<class Dest>
    void desync(Dest & dest)
    {   for(int i=0 ; i<128 ; i++)
        {   if(_ctl[i] != 0)
            {   dest.controlRel(i, -_ctl[i]);
            }
        }
    }
private:
    float _ctl[128] = {0};
};

} // bad
