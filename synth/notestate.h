#pragma once 

namespace bad {

class NoteState
{
public:
    struct Note
    {   int chan;
        int note;
        float vel;
    };

    void reset()
    {   for(int i=0 ; i<128 ; i++)
        {   _notesOn[i] = 0;
        }
    }
    void noteOn(int , int note, float vel)
    {   _notesOn[note] ++;
        _lastVel[note] = vel;
    }
    void noteOff(int , int note, float )
    {   _notesOn[note] --;
    }
    template<class Dest>
    void sync(Dest & dest)
    {   for(int i=0 ; i<128 ; i++)
        {   for(int j=0 ; j<_notesOn[i] ; j++)
            {   dest.noteOn(1, i, _lastVel[i]);
            }
        }
    }
    template<class Dest>
    void desync(Dest & dest)
    {   for(int i=0 ; i<128 ; i++)
        {   for(int j=0 ; j<_notesOn[i] ; j++)
            {   dest.noteOff(1, i, 1);
            }
        }
    }
private:
    int _notesOn[128] = {0};
    float _lastVel[128] = {0};
};

} // bad
