#pragma once

#include "arr.h"
#include "synth.h"

namespace wade {

template<class MonoSynth, int MaxPoly = 16, int BufSize = 128>
struct poly
{   
    static constexpr int num_controls() { return MonoSynth::num_controls(); }

    struct VoiceState
    {   uint16_t seq = 0;
        uint8_t on = 0;
        uint8_t note = 0;
    };

    wade::arr<MonoSynth, MaxPoly> _voices;
    wade::arr<VoiceState, MaxPoly> _states;
    wade::arr<float, BufSize> _buf;
    uint16_t _poly = MaxPoly;
    uint16_t _seq = 0;

    template<class... Args>
    poly(Args &&... args)
    :   _voices(std::forward<Args>(args)...)
    {
    }

    void set_polyphony(uint16_t poly)
    {   _poly = poly;
    }

    void note_on(uint8_t note, uint8_t vel)
    {   uint32_t bestscore = 0, besti = 0;
        for(int i=0 ; i<_poly ; i++)
        {   VoiceState & v = _states[i];
            // prefer not on, then same note, then oldest seq
            uint32_t score = (v.on == 0) << 17;
            score += (v.note == note) << 16;
            score += _seq - v.seq;
            if(score > bestscore)
            {   bestscore = score;
                besti = i;
            }
        }
        _voices[besti].note_on(note, vel);
        _states[besti].seq = _seq++;
        _states[besti].note = note;
        _states[besti].on = 1;
    }
    
    void note_off(uint8_t note, uint8_t vel)
    {   for(int i=0 ; i<MaxPoly ; i++)
        {   VoiceState & v = _states[i];
            if(v.on == 1 && v.note == note)
            {   _voices[i].note_off(note, vel);
                v.on = 0;
                break;
            }
            // TODO restore over-poly notes 
        }
    }

    void note_pressure(uint8_t note, uint8_t press)
    {   for(int i=0 ; i<MaxPoly ; i++)
        {   VoiceState & v = _states[i];
            if(v.on == 1 && v.note == note)
            {   _voices[i].note_pressure(note, press);
            }
        }
    }

    void render(float * buf, int len)
    {   wade_assert(len <= BufSize);
        for(int i=0 ; i<len ; i++) { buf[i] = 0; }
        for(int i=0 ; i<_poly ; i++)
        {   _voices[i].render(_buf.data(), len);
            for(int i=0 ; i<len ; i++) { buf[i] += _buf[i]; }
        }
    }
};


// template<class Synth, int Tracks = 4>
// struct multitrack
// {

//     wade::arr<float, 16> _voice_ctl { 0.5f };
//     wade::arr<Synth, Tracks> _tracks;
//     int _active = 0;

//     template<class... Args>
//     multitrack(Args &&... args)
//     :   _tracks(std::forward<Args>(args)...)
//     {
//     }

// };


} // wade