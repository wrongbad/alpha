#pragma once

#include "delay.h"

namespace wade {

template<int MaxSize>
struct comb_feedback : public delay<MaxSize>
{   float _fb = 0.7;
    float _y = 0;
    comb_feedback(int delay, float fb) : _fb(fb)
    {   this->set_delay(delay);
    }
    float operator()(float x, float g = 1)
    {   x += _y * _fb * g;
        _y = delay<MaxSize>::operator()(x);
        return _y;
    }
};

template<int MaxSize>
struct ap_feedback : public delay<MaxSize>
{   float _fb = 0.7;
    float _y = 0;
    ap_feedback(int delay, float fb) : _fb(fb)
    {   this->set_delay(delay);
    }
    float operator()(float x, float g = 1)
    {   x += _y * _fb * g;
        _y = delay<MaxSize>::operator()(x);
        return _y - x * _fb * g;
    }
};

struct reverb
{
    float _scale = 0.5;
    ap_feedback<512> _ap0 {347, 0.7};
    ap_feedback<128> _ap1 {113, 0.7};
    ap_feedback<64> _ap2 {37, 0.7};
    comb_feedback<2560> _comb[4] {
        {1687, 0.773}, {1601, 0.802}, 
        {2053, 0.753}, {2251, 0.733}
    };

    void set_scale(float scale) { _scale = scale; }

    float operator()(float x)
    {   x = _ap0(x, _scale * 0.6);
        x = _ap1(x, _scale * 0.6);
        x = _ap2(x, _scale * 0.6);
        float y = 0;
        for(int i=0 ; i<4 ; i++)
        {   y += _comb[i](x, _scale);
        }
        return y;
    }
};

} // wade