#pragma once

#include <cmath>

namespace wade {

// Unniversal causal linear filter class
template<int XTaps, int YTaps = 1>
struct filter
{
    float _cx[XTaps] = {0};
    float _cy[YTaps] = {1};
    float _x[XTaps] = {0};
    float _y[YTaps] = {0};

    float operator()(float x)
    {   _x[0] = x;
        _y[0] = 0;
        for(int i=0 ; i<XTaps ; i++)
        {   _y[0] += _cx[i] * _x[i];
        }
        for(int i=1 ; i<YTaps ; i++)
        {   _y[0] -= _cy[i] * _y[i];
        }
        _y[0] /= _cy[0];
        for(int i=XTaps-1 ; i>0 ; i--)
        {   _x[i] = _x[i-1];
        }
        for(int i=YTaps-1 ; i>0 ; i--)
        {   _y[i] = _y[i-1];
        }
        return _y[0];
    }
}; 


template<int N>
struct fir_damper : public filter<N>
{
    void set_rolloff(float rolloff)
    {   for(int i=0 ; i<N ; i++)
        {   this->_cx[i] = 0;
        }
        for(int j=0 ; j<=N/2 ; j++)
        {   for(int i=0 ; i<N ; i++)
            {   float x = std::pow(rolloff, j * 2.0f / N);
                x *= std::cos(i * j * M_PI * 2 / N) / N;
                x *= (j>0) + (j*2<N);
                this->_cx[i] += x;
            }
        }
    }
};


struct ap_interp
{   float _g = 0, _x = 0, _y = 0;

    void set_delay(float delay)
    {   _g = (1-delay) / (1+delay);
    }

    float operator()(float x)
    {   float y = _g * (x - _y) + _x;
        _x = x; _y = y;
        return y;
    }
};


template<int Order = 7, int Taylor = 3>
class va_filter
{
public:
    float step(float in, float dt)
    {   float y[Order + Taylor];
        for(int i=0 ; i<Order ; i++)
        {   y[i] = _state[i];
        }
        float input[Taylor] = {in, 0, 0};
        for(int i=0 ; i<Taylor ; i++)
        {   y[Order+i] = input[i];
            for(int j=0 ; j<Order ; j++)
            {   y[Order+i] -= y[i+j] * _coef[j];
            }
        }
        for(int i=0 ; i<Order ; i++)
        {   _state[i] = y[i];
            float d = 1;
            for(int j=1 ; j<=Taylor ; j++)
            {   _state[i] += y[i+j] * (d *= dt / j);
            }
        }
        return out();
    }
    float out() const
    {   return _state[0] * _coef[0];   
    }
protected:
    float _coef[Order] = {0};
    float _state[Order] = {0};
};


template<int Order = 7, int Taylor = 3>
class va_cheby1 : public va_filter<Order, Taylor>
{
public:
    // TODO constexpr
    va_cheby1(float cutoff = 1, float ripple_db = 2)
    {
        using std::sin; using std::cos;
        using std::pow; using std::tanh;
        using std::sinh; using std::cosh;
        using std::sqrt;

        float * coef = this->_coef;
        float temp[Order+1];
        for(int i=0 ; i<Order+1 ; i++)
        {   coef[i] = temp[i] = 0;
        }
        coef[0] = 1;
        float e = sqrt(pow(10.0f, ripple_db/10.0f) - 1);
        // butterworth:
        // float rad_real = 1, rad_imag = 1;
        // chebyshev-I:
        float rad_real = sinh(asinh(1/e)/Order) * cutoff;
        float rad_imag = cosh(asinh(1/e)/Order) * cutoff;
        
        for(int i=1 ; i<=Order ; i+=2)
        {   // measured CCW from +i axis
            float theta = i * M_PI / (2*Order);
            float poleR = rad_real * -sin(theta);
            float poleI = rad_imag * cos(theta);
            
            float a = 0;
            float b = 1;
            float c = -poleR;
            if(i<Order) // if pole not on real axis, add conjugate too
            {   a = 1;
                b = -poleR * 2;
                c = poleR * poleR + poleI * poleI;
            }
            // temp = polynomial(a,b,c) * coef
            for(int j=0 ; j<i ; j++)
            {   if(j+2<Order) { temp[j+2] += a * coef[j]; }
                temp[j+1] += b * coef[j];
                temp[j+0] += c * coef[j];
            }
            // copy result back to filter polynomial
            for(int j=0 ; j<Order+1 ; j++)
            {   coef[j] = temp[j];
                temp[j] = 0;
            }
        }
        // TODO
        // // normalize gain
        // float norm = 1 / sqrt(1 + e*e*((Order+1)%2));
        // std::cout << "norm = " << norm << std::endl;
        // for(int i=0 ; i<Order ; i++)
        // {   coef[i] *= norm;
        //     std::cout << "coef = " << coef[i] << std::endl;
        // }
    }
};

} // wade